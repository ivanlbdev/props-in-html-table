from service.csv_reader import CsvReader
from service.json_reader import JsonReader
from service.table_maker import TableMaker

rich_items = CsvReader('rich.csv').open_csv(['xml'])
not_rich_items = CsvReader('not_rich.csv').open_csv(['xml'])
all_items_from_site = JsonReader('catalog.json').open_json()

if __name__ == '__main__':
    rich = []
    not_reach = []
    for product in all_items_from_site:
        if product['Артикул'] in rich_items:
            rich.append(product)
        if product['Артикул'] in not_rich_items:
            not_reach.append(product)

    TableMaker(rich).save_data('rich.html', rich=True)
    TableMaker(not_reach).save_data('slow.html', rich=False)
