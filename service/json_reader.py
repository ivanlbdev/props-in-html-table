import json


class JsonReader:
    def __init__(self, file_path: str):
        self.file_path = file_path

    def open_json(self):
        with open(self.file_path, 'r') as f:
            data = json.load(f)
            return data


if __name__ == '__main__':
    JsonReader('../catalog.json').open_json()
