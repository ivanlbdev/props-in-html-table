class TableMaker:
    def __init__(self, products: list):
        self.products = products
        self.open_tag = '<table>'
        self.headers = '<tr><td>Артикул</td><td>Название</td><td>Характеристика</td><td>Значение</td></tr>'
        self.close_tag = '</table>'

    def str_make(self):
        result = self.open_tag + self.headers
        for item in self.products:
            head_data = f'<td>{item["Артикул"]}</td><td>{item["NAME"]}</td>'
            exceptions = ['Артикул', 'NAME', 'ID', 'Производитель', 'Страна производства', 'Родина бренда', 'Гарантия']
            for ex in exceptions:
                try:
                    del item[ex]
                except Exception as e:
                    print('нет такой')
            count = len(item)
            count_null = 0
            if count != 0:
                if count <= 2:
                    for key, value in item.items():
                        result += '<tr>'
                        result += head_data
                        res = f'<td>{key}</td><td>{value}</td>'
                        result += res
                        result += '</tr>'
                else:
                    check = 0
                    for key, value in item.items():
                        if check < 2:
                            result += '<tr>'
                            result += head_data
                            res = f'<td>{key}</td><td>{value}</td>'
                            result += res
                            result += '</tr>'
                            check += 1
                        else:
                            break
            else:
                count_null += 1
        print(count_null)
        result += self.close_tag
        result += f'<b>Всего характеристик на {len(self.products) - count_null} товаров</b>'
        return result

    def str_make_rich(self):
        result = self.open_tag + self.headers
        for item in self.products:
            head_data = f'<td>{item["Артикул"]}</td><td>{item["NAME"]}</td>'
            for key, value in item.items():
                if key != 'Артикул' and key != 'NAME' and key != 'ID':
                    result += '<tr>'
                    result += head_data
                    res = f'<td>{key}</td><td>{value}</td>'
                    result += res
                    result += '</tr>'
        result += self.close_tag
        result += f'<b>Всего характеристик на {len(self.products)} товаров</b>'
        return result

    def save_data(self, file: str, rich: bool):
        if rich:
            data = self.str_make_rich()
        else:
            data = self.str_make()
        with open(file, 'w', encoding='utf8') as f:
            f.write(data)
