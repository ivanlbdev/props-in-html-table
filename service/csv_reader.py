import csv


class CsvReader:
    def __init__(self, file_path: str):
        self.file_path = file_path

    def open_csv(self, props: list):
        result = []
        with open(self.file_path, 'r', encoding='utf8', newline='') as f:
            data = csv.DictReader(f)
            for row in data:
                for prop in props:
                    result.append(row[prop])
        return result


if __name__ == '__main__':
    CsvReader('../rich.csv').open_csv(['xml'])
